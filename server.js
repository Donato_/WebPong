/*jslint node: true */
var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io").listen(server);

app.use("/assets", express.static(__dirname + "/assets"));
app.use("/css", express.static(__dirname + "/build/css"));
app.use("/js", express.static(__dirname + "/build/js"));

app.get("/game", function (req, res) {
	res.sendFile(__dirname + "/views/index.html");
});

app.get("/", function (req, res) {
	res.sendFile(__dirname + "/views/index.html");
});

var port = process.env.PORT || 3000;
server.listen(port, function () {
	console.log("Listening on port " + server.address().port);
});
//---END OF CONFIG-------------------------------------------------------------

server.lastPlayerId = 0;
server.maxPlayers = 2;
server.paddles = [];

io.on("connection", function (clientSocket) {
	console.log("connected: ", clientSocket.id);
	clientSocket.on("askNewPlayer", function (clientPaddleSave) {
		var players = server.paddles.length;
		if (players < server.maxPlayers) {
			var newPaddle = {
				id: ++server.lastPlayerId,
				steps: 5
			};
			server.paddles[players] = newPaddle;
			clientSocket.paddle = newPaddle;
			clientPaddleSave(newPaddle);

			if (server.paddles.length == server.maxPlayers) {
				server.paddles.forEach((p) => {
					delete p.x;
					delete p.y;
				});
				io.emit("maxPlayersReached", server.paddles);
			}
		} else {
			if (server.paddles.filter((p) => {
					return p.side;
				}).length == server.maxPlayers) {
				clientSocket.emit("gamestart", server.paddles);
			} else {
				clientSocket.emit("maxPlayersReached", server.paddles);
			}
		}
	});

	clientSocket.on("sideChosen", function (side) {
		if (server.paddles.length != server.maxPlayers) {
			console.log("Fehlerhafte Anzahl an paddles bei Seitenwahl (" + server.paddles.length + ")");
			return;
		}

		server.paddles.forEach((p) => {
			p.side = p.id == clientSocket.paddle.id ? side : (side == "right" ? "left" : "right");
			p.color = p.side == "left" ? "blue" : "red";
			p.score = 0;
		});

		io.emit("gamestart", server.paddles);
	});

	clientSocket.on("createAllPaddles", function (createAllPaddles) {
		createAllPaddles(server.paddles);
		clientSocket.emit("updateGame", server.paddles, server.ballData ? server.ballData : undefined);
	});

	clientSocket.on("isReady", function (paddleId, randBallVelocity) {
		server.paddles.find((p) => {
			return p.id == paddleId;
		}).isReady = true;

		if (server.paddles.filter((p) => {
				return p.isReady
			}).length == server.maxPlayers) {
			if (server.ballData)
				Object.assign(server.ballData, {
					velocity: randBallVelocity
				});
			else
				server.ballData = {
					velocity: randBallVelocity
				};
			io.emit("startRound", server.ballData.velocity);
		}
	});

	clientSocket.on("moving", function (action, position) {
		if (clientSocket.paddle) {
			Object.assign(clientSocket.paddle, position);
			clientSocket.broadcast.emit("playerIsMoving", action, clientSocket.paddle);
		}
	});

	clientSocket.on("setBallState", function (position, velocity, color, lastTouchedPaddle) {
		//velocity.x = velocity.x <= 0 ? -450 : 450;
		server.ballData = {
			position: position,
			velocity: velocity,
			color: color,
			lastTouch: lastTouchedPaddle,
		};

		clientSocket.broadcast.emit("updateGame", undefined, server.ballData);
	});

	clientSocket.on("scored", function () {
		if (clientSocket.paddle) {
			clientSocket.paddle.score++;

			io.emit("updateGame", server.paddles);
			if (clientSocket.paddle.score >= 10)
				io.emit("gameover", clientSocket.paddle);
			else
				io.emit("startRound", randomBallStartVelocity());
		}
	});

	clientSocket.on("disconnect", function () {
		console.log("disconnect: ", clientSocket.id);

		if (!clientSocket.paddle)
			return;

		var paddleId = clientSocket.paddle.id;
		server.paddles.forEach((p, index, arr) => {
			if (p.id != paddleId)
				return;
			arr.splice(index, 1);
		});

		io.emit("remove", paddleId);
	});
});

function randomBallStartVelocity(xVelocity = 300, yMinVelocity = 100, yMaxVelocity = 300) {
	var randVelocity = {
		x: (Math.round(Math.random()) ? 1 : -1) * xVelocity,
		y: (Math.round(Math.random()) ? 1 : -1) * (yMinVelocity + Math.round(Math.random() * (yMaxVelocity - yMinVelocity)))
	};
	return randVelocity;
};
