module.exports = function (grunt) {
	grunt.initConfig({
		concat: {
			js: {
				src: [
					"js/client.js",
					"js/states/*.js",
					"js/game.js"
				],
				dest: "build/js/scripts.js"
			},
			css: {
				src: "css/style.css",
				dest: "build/css/styles.css"
			}
		},

		babel: {
			options: {
				presets: ["env"]
			},
			build: {
				files: [{
					src: "build/js/scripts.js",
					dest: "build/js/scripts.js"
				}]
			}
		},

		uglify: {
			options: {
				compress: true
			},
			build: {
				files: [{
					src: "build/js/scripts.js",
					dest: "build/js/scripts.min.js"
				}]
			}
		}
	});

	//Load plugins
	grunt.loadNpmTasks("grunt-babel");
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-uglify");

	//Register tasks
	grunt.registerTask("concat-js", ["concat:js"]);
	grunt.registerTask("concat-css", ["concat:css"]);
	grunt.registerTask("build", ["concat", "babel", "uglify"]);
};
