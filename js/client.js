/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var Client = {
	socket: io.connect(),

	askNewPlayer: function () {
		Client.socket.emit("askNewPlayer", function (paddle) {
			Client.paddle = paddle;
			game.state.start("waitingForPlayer");
		});
	},

	sideChosen: function (side) {
		Client.socket.emit("sideChosen", side);
	},

	createAllPaddles: function (state = game.state.getCurrentState()) {
		Client.socket.emit("createAllPaddles", function (paddles) {
			state.paddles = [];
			paddles.forEach((paddle) => {
				var newPaddle = createPaddle("paddle", paddle.side, 15, paddle);
				newPaddle.scale.x = 0.5;
				state.paddles[state.paddles.length] = newPaddle;
				if (Client.paddle && Client.paddle.id == newPaddle.id)
					Client.paddle = newPaddle;
			});

			if (Client.paddle && state.key == "play" && playState.ball && checkIfSetCorrect())
				Client.socket.emit("isReady", Client.paddle.id, randomBallStartVelocity());
		});
	},

	createBall: function (state = game.state.getCurrentState()) {
		Client.socket.emit("createBall", function (ballData) {
			state.ball.trail = createTrail("particle-" + (ballData.color ? ballData.color : "white"), ballData.position);
			state.ball = createBall("ball", "star-" + (ballData.color ? ballData.color : "white"), ballData.position, ballData.velocity);
		});
	},

	moving: function (action, x, y) {
		Client.socket.emit("moving", action, {
			x: x,
			y: y
		});
	},

	sendBallState: function (position, velocity, color, lastTouchedPaddle) {
		Client.socket.emit("setBallState", position, velocity, color, lastTouchedPaddle);
	},

	scored: function () {
		Client.socket.emit("scored");
	},
};

Client.socket.on("maxPlayersReached", function () {
	game.state.start("chooseSide");
});

Client.socket.on("gamestart", function (paddles) {
	game.state.start("play");
});

Client.socket.on("startRound", function (randBallVelocity) {
	startRound(randBallVelocity, 3);
});

Client.socket.on("playerIsMoving", function (action, movingPaddle) {
	var paddle = playState.paddles.find((p) => {
		return p.id == movingPaddle.id
	});
	if(!paddle)
		return;
	
	paddle.x = movingPaddle.x;
	paddle.y = movingPaddle.y;
	paddle.move = action;
});

Client.socket.on("updateGame", function (paddles, ballData) {
	if (paddles && playState.scoreBoard) {
		paddles.forEach((p) => {
			p.side == "left" ? playState.scoreLeft = p.score : playState.scoreRight = p.score;
		});
		playState.scoreBoard.setText(playState.scoreLeft + ":" + playState.scoreRight);
	}

	if (ballData && playState.ball) {
		Object.assign(playState.ball.position, ballData.position);
		Object.assign(playState.ball.body.velocity, ballData.velocity);
		playState.ball.lastTouch = ballData.lastTouch;
		if (playState.ball.trail)
			playState.ball.trail.particleImageKey = "particle-" + ballData.color;
	}
});

Client.socket.on("gameover", function (paddle) {
	gameoverState.winner = paddle;
	Client.socket.disconnect();

	game.state.start("gameover");
});

Client.socket.on("remove", function (id) {
	playState.removePaddle(id);
});