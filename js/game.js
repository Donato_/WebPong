/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var gameWidth = 1080;
var resolution = 16 / 9;
var game = new Phaser.Game({
	width: gameWidth,
	height: gameWidth / resolution,
	renderer: Phaser.AUTO,
	parent: document.getElementById("game-window"),
	disableVisibilityChange: true
});

game.state.add("boot", bootState);
game.state.add("load", loadState);
game.state.add("waitingForPlayer", waitingState);
game.state.add("chooseSide", chooseState);
game.state.add("play", playState);
game.state.add("gameover", gameoverState);

game.state.start("boot");

//Game functions------------------------------------------------------------
function randomBallStartVelocity(xVelocity = 300, yMinVelocity = 100, yMaxVelocity = 300) {
	var randVelocity = {
		x: (Math.round(Math.random()) ? 1 : -1) * xVelocity,
		y: (Math.round(Math.random()) ? 1 : -1) * (yMinVelocity + Math.round(Math.random() * (yMaxVelocity - yMinVelocity)))
	};
	return randVelocity;
};

function createDefaultScreen(state = game.state.getCurrentState()) {
	//---background--------------------------------------------------
	state.backgroundImage = createImageBackground("background");

	//---ball------------------------------------------------------
	state.ball = createBall("bubble", undefined, undefined, "particle-white");

	//---trail-------------------------------------------------------
	var emitter = createParticleEmitter("particle-white", state.ball.position);
	state.ball.trail = emitter;
	state.ball.bringToTop();

	emitter.timer = game.time.create(false);
	emitter.timer.loop(emitter.lifespan / emitter.maxParticles, function () {
		if (emitter.counts.totalFailed) { //performance optimization
			emitter.counts.totalFailed = 0;
			emitter.timer.events[0].delay += 10;
		}
		emitter.emitParticle(state.ball.x, state.ball.y, emitter.particleImageKey);
	});
	emitter.timer.start();
}

function checkIfSetCorrect() {
	var isCorrect = false;
	if (playState.paddles.length < 2)
		game.state.start("waitingForPlayer");
	else {
		var selectedSidesCount = playState.paddles.filter((p) => {
			return p.side;
		}).length;

		if (selectedSidesCount < 2)
			game.state.start("chooseSide");
		else
			isCorrect = true;
	}

	return isCorrect;
};

function startRound(randBallVelocity, countDown = 3) {
	if (!playState.ball || !playState.ball.position || !playState.ball.body.velocity)
		return;

	Object.assign(playState.ball.position, {
		x: game.world.centerX,
		y: game.world.centerY,
	});
	Object.assign(playState.ball.body.velocity, randBallVelocity);
	game.physics.arcade.isPaused = true;

	var darkBackground = game.add.graphics(0, 0);
	darkBackground.beginFill(0x000000, 0.7);
	darkBackground.drawRect(0, 0, game.width, game.height);
	darkBackground.endFill();

	var countDownText = createText("Sei bereit!");
	var timer = game.time.create(false);
	timer.loop(1000, function () {
		if (countDown)
			countDownText.setText(countDown--)
		else {
			countDownText.destroy();
			darkBackground.destroy();
			timer.destroy();

			game.physics.arcade.isPaused = false;
		}
	});
	timer.start();
}

//Create Functions_________________________________________________________
//---Text---------------------------------------------------------------
var defaultTextStyleConfig = {
	font: "bold 5em Courier",
	fill: "#ffffff",
	boundsAlignH: "center",
	boundsAlignV: "middle",
	align: "center",
};

function createText(text, customTextStyleConfig = {}) {
	var label = game.add.text(0, 0, text, Object.assign({}, defaultTextStyleConfig, customTextStyleConfig));
	label.setTextBounds(0, 0, game.width, game.height);

	return label;
}

//---Background---------------------------------------------------------
function createImageBackground(imageKey) {
	var background = game.add.image(0, 0, imageKey);
	background.height = game.height;
	background.width = game.width;

	return background;
}

//---Ball---------------------------------------------------------------
var defaultBallSpriteConfig = {
	bouncyness: 1,
	childScaleFactor: 1,
}

function createBall(imageKey, position = {
	x: game.world.centerX,
	y: game.world.centerY
}, velocity = {
	x: 400,
	y: 200
}, childImageKey = null, customBallSpriteConfig = {}) {
	var config = Object.assign({}, defaultBallSpriteConfig, customBallSpriteConfig);

	var ballSprite = game.add.sprite(position.x, position.y, imageKey);
	ballSprite.anchor.setTo(0.5);

	game.physics.arcade.enable(ballSprite);
	ballSprite.body.setCircle(false);
	ballSprite.body.collideWorldBounds = true;
	ballSprite.body.bounce.setTo(config.bouncyness);
	ballSprite.body.velocity.set(velocity.x, velocity.y);
	ballSprite.body.onWorldBounds = new Phaser.Signal();
	ballSprite.body.onWorldBounds.add(ballHitWorldBounds);

	if (!childImageKey)
		return ballSprite;

	var childSprite = game.make.sprite(0, 0, childImageKey);
	childSprite.anchor.setTo(0.5);
	childSprite.width = ballSprite.width * config.childScaleFactor;
	childSprite.height = ballSprite.height * config.childScaleFactor;
	ballSprite.addChild(childSprite);

	return ballSprite;
};

//---Emitter------------------------------------------------------------
var defaultParticleEmitterConfig = {
	maxParticles: 100,
	startSize: 0.5,
	endSize: 0,
	lifespan: 2000,
	gravity: 0,
	speed: 25,
}

function createParticleEmitter(imageKey, position, customParticleEmitterConfig = {}) {
	var config = Object.assign({}, defaultParticleEmitterConfig, customParticleEmitterConfig);

	var emitter = game.add.emitter(position.x, position.y, config.maxParticles);
	emitter.particleImageKey = imageKey;
	emitter.makeParticles(imageKey);
	emitter.gravity = config.gravity;
	emitter.setScale(config.startSize, config.endSize, config.startSize, config.endSize, config.lifespan);
	emitter.setXSpeed(-config.speed, config.speed);
	emitter.setYSpeed(-config.speed, config.speed);

	return emitter;
};

//---Paddle-------------------------------------------------------------
//var defaultPaddleConfig = {
//	offset: 50,
//}

function createPaddle(imageKey, side, offset = 15, paddleData = {}) {
	//var config = Object.assign({}, defaultPaddleConfig, customPaddleConfig);

	var paddle = game.add.sprite(0, 0, imageKey);
	paddle.anchor.setTo(0, 0.5);

	paddle.side = side;
	switch (side) {
		case "left":
			paddle.x = offset;
			paddle.y = game.world.centerY;
			break;
		case "right":
			//			paddle.scale.set(-1);
			paddle.anchor.setTo(1, 0.5);
			paddle.x = game.width - offset;
			paddle.y = game.world.centerY;
			break;
		case "top":
			paddle.angle = 90;
			paddle.x = game.world.centerX;
			paddle.y = offset;
			break;
		case "bottom":
			paddle.angle = -90;
			paddle.x = game.world.centerX;
			paddle.y = game.height - offset;
			break;
	}

	game.physics.arcade.enable(paddle);
	paddle.body.immovable = true;

	Object.assign(paddle, paddleData);

	//	var circle = game.add.graphics(0, 0);
	//	circle.beginFill(0x00ff00, 1);
	//	circle.drawRoundedRect(0, 0, 10, 10, 5);
	//	circle.endFill();
	//
	//	paddle.addChild(circle);

	return paddle;
};

//---Selection Box------------------------------------------------------
var defaultSideSelectionBoxConfig = {
	alphaFill: 0.5,
}

function createSideSelectionBox(side, color, onInputDownFunction = null, customSideSelectionBoxConfig = {}) {
	var config = Object.assign({}, defaultSideSelectionBoxConfig, customSideSelectionBoxConfig);

	var rectanlge = game.add.graphics(game.width * 0.5, game.height * 0.5);
	var boxWidth = game.width * 0.45;
	var boxHeight = game.height * 0.45;

	switch (side) {
		case "left":
			rectanlge.x *= 0.25;
			boxWidth *= 0.5;
			break;
		case "right":
			rectanlge.x *= 1.75;
			boxWidth *= 0.5;
			break;
		case "top":
			rectanlge.y *= 0.25;
			boxHeight *= 0.5;
			break;
		case "bottom":
			rectanlge.y *= 1.75;
			boxHeight *= 0.5;
			break;
	}

	rectanlge.lineStyle(2, color, 1);
	rectanlge.beginFill(color, config.alphaFill);
	rectanlge.drawRoundedRect(-boxWidth * 0.5, -boxHeight * 0.5, boxWidth, boxHeight, 25);
	rectanlge.endFill();

	if (onInputDownFunction) {
		rectanlge.inputEnabled = true;
		rectanlge.events.onInputDown.add(onInputDownFunction);
	}

	return rectanlge;
};

//Collition Handlers_______________________________________________________
function ballHitWorldBounds(ball, up, down, left, right) {
	if (!Client.paddle || up || down || !Client.paddle.side)
		return;

	if ((Client.paddle.side == "left" && right) || (Client.paddle.side == "right" && left))
		Client.scored();
}

//DEBUG____________________________________________________________________
var showDebug = false;

function toggleDebug() {
	showDebug = !showDebug;

	if (!showDebug)
		game.debug.reset();
}
