/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var loadState = {
	preload: function () {
		createText("loading...");

		game.load.image("paddle", "assets/images/paddles/paddle-hearthstonelike.png");

		game.load.image("background", "assets/images/backgrounds/space.png");

		game.load.image("particle-white", "assets/images/particles/white.png");
		game.load.image("particle-blue", "assets/images/particles/blue.png");
		game.load.image("particle-red", "assets/images/particles/red.png");

		//game.load.image("loop-gold", "assets/images/misc/loop-gold.png");

		//game.load.image("star-white", "assets/images/stars/star-white.png");
		//game.load.image("star-blue", "assets/images/stars/star-blue.png");
		//game.load.image("star-red", "assets/images/stars/star-red.png");

		game.load.image("bubble", "assets/images/balls/bubble.png");
	},

	create: function () {
		Client.askNewPlayer();
	}
};
