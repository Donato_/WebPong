/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var bootState = {
	create: function () {
		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		game.scale.pageAlignHorizontally = true;
		game.scale.pageAlignVertically = true;
		game.state.start("load");
	}
};
