/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var waitingState = {
	create: function () {
		createDefaultScreen();
		createText((Client.paddle ? "(Spieler)" : "(Zuschauer)") + "\rWarten auf Spieler...");
	}
};