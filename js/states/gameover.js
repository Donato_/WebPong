/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var gameoverState = {
	create: function () {
		createDefaultScreen();

		var winnerColor = playState.paddles.find((p) => {
			return p.side == gameoverState.winner.side;
		}).color;

		gameoverState.ball.trail.particleImageKey = "particle-" + winnerColor;
		createText("GAME OVER\rDer Gewinner ist Spieler " + (winnerColor == "blue" ? "Blau" : "Rot"));
	}
};
