/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var counter = 0;
var updateCounter = 0;
var playState = {
	paddles: [],

	create: function () {
		createDefaultScreen();
		playState.scoreBoard = createText("0:0", {
			boundsAlignV: "top"
		});

		//---paddles-----------------------------------------------------
		Client.createAllPaddles(playState);

		playState.cursors = game.input.keyboard.createCursorKeys();

		//---DEBUG-------------------------------------------------------
		game.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		game.spaceKey.onDown.add(toggleDebug, this);
	},

	update: function () {
		game.physics.arcade.collide(playState.paddles, playState.ball, ballHitPaddle);

		playState.paddles.filter((p) => {
			return p.hasOwnProperty("move");
		}).forEach(function (paddle) {
			var yMin = paddle.body.halfHeight;
			var yMax = game.height - paddle.body.halfHeight;
			if (paddle.move == "Up") {
				movedPaddleUp(paddle);
			} else if (paddle.move == "Down") {
				movedPaddleDown(paddle);
			}
		});

		var paddle = Client.paddle;
		if (!paddle)
			return;

		var activePointer = game.input.activePointer;
		var pointerIsInUpperHalf = activePointer.pageY <= document.body.clientHeight * 0.5 ? true : false;
		var upActionActive = playState.cursors.up.isDown || (activePointer.isDown && pointerIsInUpperHalf);
		var downActionActive = playState.cursors.down.isDown || (activePointer.isDown && !pointerIsInUpperHalf);
		
		if (upActionActive ^ downActionActive) {
			if (upActionActive) {
				movedPaddleUp(paddle);
				
				if (!Client.wasMovingUp) {
					Client.wasMovingUp = true;
					Client.wasMovingDown = false;
					Client.moving("Up", paddle.x, paddle.y);
				}
			} else if (downActionActive) {
				movedPaddleDown(paddle);

				if (!Client.wasMovingDown) {
					Client.wasMovingUp = false;
					Client.wasMovingDown = true;
					Client.moving("Down", paddle.x, paddle.y);
				}
			}
		} else {
			if (Client.wasMovingUp || Client.wasMovingDown) {
				Client.moving("Stopped", paddle.x, paddle.y);
				Client.wasMovingUp = false;
				Client.wasMovingDown = false;
			}
		}
	},

	removePaddle: function (id) {
		playState.paddles.forEach((p, index, arr) => {
			if (p.id != id)
				return;
			p.destroy();
			arr.splice(index, 1);
		});
	},

	render: function () {
		if (showDebug) {
			game.debug.bodyInfo(playState.ball, 150, 100);
			game.debug.body(playState.ball);

			playState.paddles.forEach((p, i) => {
				game.debug.bodyInfo(p, 150, 100 + (i + 1) * 200);
				game.debug.body(p);
			});
		}
	}
};

function movedPaddleUp(paddle) {
	var yMin = paddle.body.halfHeight;
	var yMax = game.height - paddle.body.halfHeight;
	if (paddle.y != yMin) {
		if (paddle.y > yMin)
			paddle.y -= paddle.steps;
		else
			paddle.y = yMin;
	}
}

function movedPaddleDown(paddle) {
	var yMin = paddle.body.halfHeight;
	var yMax = game.height - paddle.body.halfHeight;
	if (paddle.y != yMax) {
		if (paddle.y < yMax)
			paddle.y += paddle.steps;
		else
			paddle.y = yMax;
	}
}

function ballHitPaddle(paddle, ball) {
	if (Client.paddle && Client.paddle.id == paddle.id) {

		playState.ball.trail.particleImageKey = "particle-" + paddle.color;
		ball.body.velocity.x += ball.body.velocity.x > 0 ? 10 : -10;
		Client.sendBallState(ball.position, ball.body.velocity, paddle.color, paddle.side);
	}
}
