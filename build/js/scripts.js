"use strict";

/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var Client = {
	socket: io.connect(),

	askNewPlayer: function askNewPlayer() {
		Client.socket.emit("askNewPlayer", function (paddle) {
			Client.paddle = paddle;
			game.state.start("waitingForPlayer");
		});
	},

	sideChosen: function sideChosen(side) {
		Client.socket.emit("sideChosen", side);
	},

	createAllPaddles: function createAllPaddles() {
		var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : game.state.getCurrentState();

		Client.socket.emit("createAllPaddles", function (paddles) {
			state.paddles = [];
			paddles.forEach(function (paddle) {
				var newPaddle = createPaddle("paddle", paddle.side, 15, paddle);
				newPaddle.scale.x = 0.5;
				state.paddles[state.paddles.length] = newPaddle;
				if (Client.paddle && Client.paddle.id == newPaddle.id) Client.paddle = newPaddle;
			});

			if (Client.paddle && state.key == "play" && playState.ball && checkIfSetCorrect()) Client.socket.emit("isReady", Client.paddle.id, randomBallStartVelocity());
		});
	},

	createBall: function createBall() {
		var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : game.state.getCurrentState();

		Client.socket.emit("createBall", function (ballData) {
			state.ball.trail = createTrail("particle-" + (ballData.color ? ballData.color : "white"), ballData.position);
			state.ball = _createBall("ball", "star-" + (ballData.color ? ballData.color : "white"), ballData.position, ballData.velocity);
		});
	},

	moving: function moving(action, x, y) {
		Client.socket.emit("moving", action, {
			x: x,
			y: y
		});
	},

	sendBallState: function sendBallState(position, velocity, color, lastTouchedPaddle) {
		Client.socket.emit("setBallState", position, velocity, color, lastTouchedPaddle);
	},

	scored: function scored() {
		Client.socket.emit("scored");
	}
};

Client.socket.on("maxPlayersReached", function () {
	game.state.start("chooseSide");
});

Client.socket.on("gamestart", function (paddles) {
	game.state.start("play");
});

Client.socket.on("startRound", function (randBallVelocity) {
	startRound(randBallVelocity, 3);
});

Client.socket.on("playerIsMoving", function (action, movingPaddle) {
	var paddle = playState.paddles.find(function (p) {
		return p.id == movingPaddle.id;
	});
	if (!paddle) return;

	paddle.x = movingPaddle.x;
	paddle.y = movingPaddle.y;
	paddle.move = action;
});

Client.socket.on("updateGame", function (paddles, ballData) {
	if (paddles && playState.scoreBoard) {
		paddles.forEach(function (p) {
			p.side == "left" ? playState.scoreLeft = p.score : playState.scoreRight = p.score;
		});
		playState.scoreBoard.setText(playState.scoreLeft + ":" + playState.scoreRight);
	}

	if (ballData && playState.ball) {
		Object.assign(playState.ball.position, ballData.position);
		Object.assign(playState.ball.body.velocity, ballData.velocity);
		playState.ball.lastTouch = ballData.lastTouch;
		if (playState.ball.trail) playState.ball.trail.particleImageKey = "particle-" + ballData.color;
	}
});

Client.socket.on("gameover", function (paddle) {
	gameoverState.winner = paddle;
	Client.socket.disconnect();

	game.state.start("gameover");
});

Client.socket.on("remove", function (id) {
	playState.removePaddle(id);
});
/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var bootState = {
	create: function create() {
		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		game.scale.pageAlignHorizontally = true;
		game.scale.pageAlignVertically = true;
		game.state.start("load");
	}
};

/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var chooseState = {
	create: function create() {
		createDefaultScreen();

		if (Client.paddle) {
			chooseState.leftRect = createSideSelectionBox("left", 0x0000ff, onLeftSideSelected);
			chooseState.rightRect = createSideSelectionBox("right", 0xff0000, onRightSideSelected);
			createText("Wähle eine Seite");
		} else createText("Warten auf Spieler-Seitenauswahl...");
	}
};

function onLeftSideSelected() {
	Client.sideChosen("left");
};

function onRightSideSelected() {
	Client.sideChosen("right");
};

/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var gameoverState = {
	create: function create() {
		createDefaultScreen();

		var winnerColor = playState.paddles.find(function (p) {
			return p.side == gameoverState.winner.side;
		}).color;

		gameoverState.ball.trail.particleImageKey = "particle-" + winnerColor;
		createText("GAME OVER\rDer Gewinner ist Spieler " + (winnerColor == "blue" ? "Blau" : "Rot"));
	}
};

/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var loadState = {
	preload: function preload() {
		createText("loading...");

		game.load.image("paddle", "assets/images/paddles/paddle-hearthstonelike.png");

		game.load.image("background", "assets/images/backgrounds/space.png");

		game.load.image("particle-white", "assets/images/particles/white.png");
		game.load.image("particle-blue", "assets/images/particles/blue.png");
		game.load.image("particle-red", "assets/images/particles/red.png");

		//game.load.image("loop-gold", "assets/images/misc/loop-gold.png");

		//game.load.image("star-white", "assets/images/stars/star-white.png");
		//game.load.image("star-blue", "assets/images/stars/star-blue.png");
		//game.load.image("star-red", "assets/images/stars/star-red.png");

		game.load.image("bubble", "assets/images/balls/bubble.png");
	},

	create: function create() {
		Client.askNewPlayer();
	}
};

/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var counter = 0;
var updateCounter = 0;
var playState = {
	paddles: [],

	create: function create() {
		createDefaultScreen();
		playState.scoreBoard = createText("0:0", {
			boundsAlignV: "top"
		});

		//---paddles-----------------------------------------------------
		Client.createAllPaddles(playState);

		playState.cursors = game.input.keyboard.createCursorKeys();

		//---DEBUG-------------------------------------------------------
		game.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		game.spaceKey.onDown.add(toggleDebug, this);
	},

	update: function update() {
		game.physics.arcade.collide(playState.paddles, playState.ball, ballHitPaddle);

		playState.paddles.filter(function (p) {
			return p.hasOwnProperty("move");
		}).forEach(function (paddle) {
			var yMin = paddle.body.halfHeight;
			var yMax = game.height - paddle.body.halfHeight;
			if (paddle.move == "Up") {
				movedPaddleUp(paddle);
			} else if (paddle.move == "Down") {
				movedPaddleDown(paddle);
			}
		});

		var paddle = Client.paddle;
		if (!paddle) return;

		var activePointer = game.input.activePointer;
		var pointerIsInUpperHalf = activePointer.pageY <= document.body.clientHeight * 0.5 ? true : false;
		var upActionActive = playState.cursors.up.isDown || activePointer.isDown && pointerIsInUpperHalf;
		var downActionActive = playState.cursors.down.isDown || activePointer.isDown && !pointerIsInUpperHalf;

		if (upActionActive ^ downActionActive) {
			if (upActionActive) {
				movedPaddleUp(paddle);

				if (!Client.wasMovingUp) {
					Client.wasMovingUp = true;
					Client.wasMovingDown = false;
					Client.moving("Up", paddle.x, paddle.y);
				}
			} else if (downActionActive) {
				movedPaddleDown(paddle);

				if (!Client.wasMovingDown) {
					Client.wasMovingUp = false;
					Client.wasMovingDown = true;
					Client.moving("Down", paddle.x, paddle.y);
				}
			}
		} else {
			if (Client.wasMovingUp || Client.wasMovingDown) {
				Client.moving("Stopped", paddle.x, paddle.y);
				Client.wasMovingUp = false;
				Client.wasMovingDown = false;
			}
		}
	},

	removePaddle: function removePaddle(id) {
		playState.paddles.forEach(function (p, index, arr) {
			if (p.id != id) return;
			p.destroy();
			arr.splice(index, 1);
		});
	},

	render: function render() {
		if (showDebug) {
			game.debug.bodyInfo(playState.ball, 150, 100);
			game.debug.body(playState.ball);

			playState.paddles.forEach(function (p, i) {
				game.debug.bodyInfo(p, 150, 100 + (i + 1) * 200);
				game.debug.body(p);
			});
		}
	}
};

function movedPaddleUp(paddle) {
	var yMin = paddle.body.halfHeight;
	var yMax = game.height - paddle.body.halfHeight;
	if (paddle.y != yMin) {
		if (paddle.y > yMin) paddle.y -= paddle.steps;else paddle.y = yMin;
	}
}

function movedPaddleDown(paddle) {
	var yMin = paddle.body.halfHeight;
	var yMax = game.height - paddle.body.halfHeight;
	if (paddle.y != yMax) {
		if (paddle.y < yMax) paddle.y += paddle.steps;else paddle.y = yMax;
	}
}

function ballHitPaddle(paddle, ball) {
	if (Client.paddle && Client.paddle.id == paddle.id) {

		playState.ball.trail.particleImageKey = "particle-" + paddle.color;
		ball.body.velocity.x += ball.body.velocity.x > 0 ? 10 : -10;
		Client.sendBallState(ball.position, ball.body.velocity, paddle.color, paddle.side);
	}
}

/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var waitingState = {
	create: function create() {
		createDefaultScreen();
		createText((Client.paddle ? "(Spieler)" : "(Zuschauer)") + "\rWarten auf Spieler...");
	}
};
/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var gameWidth = 1080;
var resolution = 16 / 9;
var game = new Phaser.Game({
	width: gameWidth,
	height: gameWidth / resolution,
	renderer: Phaser.AUTO,
	parent: document.getElementById("game-window"),
	disableVisibilityChange: true
});

game.state.add("boot", bootState);
game.state.add("load", loadState);
game.state.add("waitingForPlayer", waitingState);
game.state.add("chooseSide", chooseState);
game.state.add("play", playState);
game.state.add("gameover", gameoverState);

game.state.start("boot");

//Game functions------------------------------------------------------------
function randomBallStartVelocity() {
	var xVelocity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 300;
	var yMinVelocity = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 100;
	var yMaxVelocity = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;

	var randVelocity = {
		x: (Math.round(Math.random()) ? 1 : -1) * xVelocity,
		y: (Math.round(Math.random()) ? 1 : -1) * (yMinVelocity + Math.round(Math.random() * (yMaxVelocity - yMinVelocity)))
	};
	return randVelocity;
};

function createDefaultScreen() {
	var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : game.state.getCurrentState();

	//---background--------------------------------------------------
	state.backgroundImage = createImageBackground("background");

	//---ball------------------------------------------------------
	state.ball = _createBall("bubble", undefined, undefined, "particle-white");

	//---trail-------------------------------------------------------
	var emitter = createParticleEmitter("particle-white", state.ball.position);
	state.ball.trail = emitter;
	state.ball.bringToTop();

	emitter.timer = game.time.create(false);
	emitter.timer.loop(emitter.lifespan / emitter.maxParticles, function () {
		if (emitter.counts.totalFailed) {
			//performance optimization
			emitter.counts.totalFailed = 0;
			emitter.timer.events[0].delay += 10;
		}
		emitter.emitParticle(state.ball.x, state.ball.y, emitter.particleImageKey);
	});
	emitter.timer.start();
}

function checkIfSetCorrect() {
	var isCorrect = false;
	if (playState.paddles.length < 2) game.state.start("waitingForPlayer");else {
		var selectedSidesCount = playState.paddles.filter(function (p) {
			return p.side;
		}).length;

		if (selectedSidesCount < 2) game.state.start("chooseSide");else isCorrect = true;
	}

	return isCorrect;
};

function startRound(randBallVelocity) {
	var countDown = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 3;

	if (!playState.ball || !playState.ball.position || !playState.ball.body.velocity) return;

	Object.assign(playState.ball.position, {
		x: game.world.centerX,
		y: game.world.centerY
	});
	Object.assign(playState.ball.body.velocity, randBallVelocity);
	game.physics.arcade.isPaused = true;

	var darkBackground = game.add.graphics(0, 0);
	darkBackground.beginFill(0x000000, 0.7);
	darkBackground.drawRect(0, 0, game.width, game.height);
	darkBackground.endFill();

	var countDownText = createText("Sei bereit!");
	var timer = game.time.create(false);
	timer.loop(1000, function () {
		if (countDown) countDownText.setText(countDown--);else {
			countDownText.destroy();
			darkBackground.destroy();
			timer.destroy();

			game.physics.arcade.isPaused = false;
		}
	});
	timer.start();
}

//Create Functions_________________________________________________________
//---Text---------------------------------------------------------------
var defaultTextStyleConfig = {
	font: "bold 5em Courier",
	fill: "#ffffff",
	boundsAlignH: "center",
	boundsAlignV: "middle",
	align: "center"
};

function createText(text) {
	var customTextStyleConfig = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

	var label = game.add.text(0, 0, text, Object.assign({}, defaultTextStyleConfig, customTextStyleConfig));
	label.setTextBounds(0, 0, game.width, game.height);

	return label;
}

//---Background---------------------------------------------------------
function createImageBackground(imageKey) {
	var background = game.add.image(0, 0, imageKey);
	background.height = game.height;
	background.width = game.width;

	return background;
}

//---Ball---------------------------------------------------------------
var defaultBallSpriteConfig = {
	bouncyness: 1,
	childScaleFactor: 1
};

function _createBall(imageKey) {
	var position = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
		x: game.world.centerX,
		y: game.world.centerY
	};
	var velocity = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
		x: 400,
		y: 200
	};
	var childImageKey = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
	var customBallSpriteConfig = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

	var config = Object.assign({}, defaultBallSpriteConfig, customBallSpriteConfig);

	var ballSprite = game.add.sprite(position.x, position.y, imageKey);
	ballSprite.anchor.setTo(0.5);

	game.physics.arcade.enable(ballSprite);
	ballSprite.body.setCircle(false);
	ballSprite.body.collideWorldBounds = true;
	ballSprite.body.bounce.setTo(config.bouncyness);
	ballSprite.body.velocity.set(velocity.x, velocity.y);
	ballSprite.body.onWorldBounds = new Phaser.Signal();
	ballSprite.body.onWorldBounds.add(ballHitWorldBounds);

	if (!childImageKey) return ballSprite;

	var childSprite = game.make.sprite(0, 0, childImageKey);
	childSprite.anchor.setTo(0.5);
	childSprite.width = ballSprite.width * config.childScaleFactor;
	childSprite.height = ballSprite.height * config.childScaleFactor;
	ballSprite.addChild(childSprite);

	return ballSprite;
};

//---Emitter------------------------------------------------------------
var defaultParticleEmitterConfig = {
	maxParticles: 100,
	startSize: 0.5,
	endSize: 0,
	lifespan: 2000,
	gravity: 0,
	speed: 25
};

function createParticleEmitter(imageKey, position) {
	var customParticleEmitterConfig = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	var config = Object.assign({}, defaultParticleEmitterConfig, customParticleEmitterConfig);

	var emitter = game.add.emitter(position.x, position.y, config.maxParticles);
	emitter.particleImageKey = imageKey;
	emitter.makeParticles(imageKey);
	emitter.gravity = config.gravity;
	emitter.setScale(config.startSize, config.endSize, config.startSize, config.endSize, config.lifespan);
	emitter.setXSpeed(-config.speed, config.speed);
	emitter.setYSpeed(-config.speed, config.speed);

	return emitter;
};

//---Paddle-------------------------------------------------------------
//var defaultPaddleConfig = {
//	offset: 50,
//}

function createPaddle(imageKey, side) {
	var offset = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 15;
	var paddleData = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

	//var config = Object.assign({}, defaultPaddleConfig, customPaddleConfig);

	var paddle = game.add.sprite(0, 0, imageKey);
	paddle.anchor.setTo(0, 0.5);

	paddle.side = side;
	switch (side) {
		case "left":
			paddle.x = offset;
			paddle.y = game.world.centerY;
			break;
		case "right":
			//			paddle.scale.set(-1);
			paddle.anchor.setTo(1, 0.5);
			paddle.x = game.width - offset;
			paddle.y = game.world.centerY;
			break;
		case "top":
			paddle.angle = 90;
			paddle.x = game.world.centerX;
			paddle.y = offset;
			break;
		case "bottom":
			paddle.angle = -90;
			paddle.x = game.world.centerX;
			paddle.y = game.height - offset;
			break;
	}

	game.physics.arcade.enable(paddle);
	paddle.body.immovable = true;

	Object.assign(paddle, paddleData);

	//	var circle = game.add.graphics(0, 0);
	//	circle.beginFill(0x00ff00, 1);
	//	circle.drawRoundedRect(0, 0, 10, 10, 5);
	//	circle.endFill();
	//
	//	paddle.addChild(circle);

	return paddle;
};

//---Selection Box------------------------------------------------------
var defaultSideSelectionBoxConfig = {
	alphaFill: 0.5
};

function createSideSelectionBox(side, color) {
	var onInputDownFunction = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	var customSideSelectionBoxConfig = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

	var config = Object.assign({}, defaultSideSelectionBoxConfig, customSideSelectionBoxConfig);

	var rectanlge = game.add.graphics(game.width * 0.5, game.height * 0.5);
	var boxWidth = game.width * 0.45;
	var boxHeight = game.height * 0.45;

	switch (side) {
		case "left":
			rectanlge.x *= 0.25;
			boxWidth *= 0.5;
			break;
		case "right":
			rectanlge.x *= 1.75;
			boxWidth *= 0.5;
			break;
		case "top":
			rectanlge.y *= 0.25;
			boxHeight *= 0.5;
			break;
		case "bottom":
			rectanlge.y *= 1.75;
			boxHeight *= 0.5;
			break;
	}

	rectanlge.lineStyle(2, color, 1);
	rectanlge.beginFill(color, config.alphaFill);
	rectanlge.drawRoundedRect(-boxWidth * 0.5, -boxHeight * 0.5, boxWidth, boxHeight, 25);
	rectanlge.endFill();

	if (onInputDownFunction) {
		rectanlge.inputEnabled = true;
		rectanlge.events.onInputDown.add(onInputDownFunction);
	}

	return rectanlge;
};

//Collition Handlers_______________________________________________________
function ballHitWorldBounds(ball, up, down, left, right) {
	if (!Client.paddle || up || down || !Client.paddle.side) return;

	if (Client.paddle.side == "left" && right || Client.paddle.side == "right" && left) Client.scored();
}

//DEBUG____________________________________________________________________
var showDebug = false;

function toggleDebug() {
	showDebug = !showDebug;

	if (!showDebug) game.debug.reset();
}
