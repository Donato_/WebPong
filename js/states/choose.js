/*global Phaser, bootState, loadState, waitingState, chooseState, playState, gameoverState, io, game, Client, */

var chooseState = {
	create: function () {
		createDefaultScreen();

		if (Client.paddle) {
			chooseState.leftRect = createSideSelectionBox("left", 0x0000ff, onLeftSideSelected);
			chooseState.rightRect = createSideSelectionBox("right", 0xff0000, onRightSideSelected);
			createText("Wähle eine Seite");
		} else
			createText("Warten auf Spieler-Seitenauswahl...");
	}
};

function onLeftSideSelected() {
	Client.sideChosen("left");
};

function onRightSideSelected() {
	Client.sideChosen("right");
};
